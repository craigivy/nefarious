package main

import (
	"bufio"
	"fmt"
	"github.com/namsral/flag"
	"log"
	"math"
	"math/rand"
	"net/http"
	"os"
	"time"
)

func main() {

	var mode string

	flag.StringVar(&mode, "mode", "default", "infinite for initite loop of simple addition to stress the CPU")
	flag.Parse()

	log.Printf("Mode is: %s", mode)
	url := "http://google.com"
	resp, err := http.Get("http://google.com/")
	logFatal(err, "Error getting"+url)
	if err != nil {
		log.Fatalf("%s: %s", "Error getting"+url, err)
	} else {
		log.Printf("Returned %s from %s", resp.Status, url)
	}

	filepath := "/tmp/blah.txt"

	start := time.Now()
	createRandomFile(filepath, 10)
	elapsed := time.Since(start)

	f, err := os.Open(filepath)
	fi, err := f.Stat()
	logFatal(err, "Error getting file info")

	log.Printf("Nafarious took %s to write the file %s of size %s", elapsed, fi.Name(), bToa(fi.Size()))

	os.Remove(fi.Name())

	if "infinite" == mode {
		log.Println("inifinite loop time. To exit press CTRL+C")
		forever := make(chan bool)
		go infinite()
		<-forever
	}
}

func infinite() {
	var i int64 = 0
	for {
		i++
		if i > 1000000 {
			i = 0
		}
	}
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func createRandomFile(name string, sizeInMB int) {
	f, err := os.Create(name)
	logFatal(err, "Error creating file")
	defer f.Close()

	for i := 0; i < sizeInMB; i++ {
		w := bufio.NewWriter(f)
		_, err := w.WriteString(randStringBytes(1000000))
		logFatal(err, "Error writing to file")
		// fmt.Printf("wrote %d bytes\n", n4)
		w.Flush()
	}
}

func bToa(s int64) string {
	sizes := []string{"B", "kB", "MB", "GB", "TB", "PB", "EB"}
	return humanateBytes(uint64(s), 1000, sizes)
}

func humanateBytes(s uint64, base float64, sizes []string) string {
	if s < 10 {
		return fmt.Sprintf("%d B", s)
	}
	e := math.Floor(logn(float64(s), base))
	suffix := sizes[int(e)]
	val := math.Floor(float64(s)/math.Pow(base, e)*10+0.5) / 10
	f := "%.0f %s"
	if val < 10 {
		f = "%.1f %s"
	}

	return fmt.Sprintf(f, val, suffix)
}

func logn(n, b float64) float64 {
	return math.Log(n) / math.Log(b)
}

func logFatal(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
