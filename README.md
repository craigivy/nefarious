# nefarious
Nefarious is a parasite that must be contained within a host
A simple go lang program to be used to validate how docker can contain a container.  Writes a 10 mb file in an inefficient manner
    
### Docker build
    docker build -t  nefarious .
       
### Run
#### Running example
    docker run -e "MODE=default" --cpus="1" --network="bridge" --memory=10m --read-only --tmpfs /run --tmpfs /tmp:size=25m nefarious --rm nefarious
### Limiting resources
* Adjust cpu and notice how much longer it takes 
``` 
    --cpus=".1"
```    
* Adjust the memory and notice the program is killed
```
    --memory=4m
```     
* Adjust the filesystem and notice the error 
```   
    --tmpfs /tmp:size=5m
```     
* Adjust the network setting and notice the result of the http get
```
     --network="none"
```     
* Adjust the mode to stress the CPU, runs an infinite loop of addition
```
    -e "MODE=infinite"
```     

## Docker metrics
 
    docker stats